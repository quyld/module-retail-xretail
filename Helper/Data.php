<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 20/06/2016
 * Time: 14:35
 */

namespace SM\XRetail\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(\Magento\Framework\App\Helper\Context $context)
    {
        parent::__construct($context);
    }

    public function addLog($mess, $level = null, $file = null)
    {
        if (is_null($file)) {
            $file = 'xRetail_api.log';
        }
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $file);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        return $logger->log($level, $mess);
    }

    public function arraySumIdenticalKeys() {
        $arrays      = func_get_args();
        $arrayReduce = array_reduce(
            $arrays,
            function ($keys, $arr) {
                $a = $keys + $arr;

                return $a;
            },
            []);
        $keys        = array_keys($arrayReduce);
        $sums        = [];

        foreach ($keys as $key) {
            $sums[$key] = array_reduce($arrays, function ($sum, $arr) use ($key) { return $sum + @$arr[$key]; });
        }

        return $sums;
    }

    /**
     * Wrap magento get Config
     *
     * @param $path
     *
     * @return mixed
     */
    public function getStoreConfig($path) {
        return $this->scopeConfig->getValue($path);
    }
}